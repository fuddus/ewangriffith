<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

	</div>

</div>

<?php if ( $wp_inspired_footer_widgets == 'Yes' ) { ?>
<?php include (TEMPLATEPATH . '/footer-widgets.php'); ?>
<?php } ?>

<div id="footer" class="clearfix">

	<div class="footer-content clearfix">

		&copy; <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a> <?php echo date('Y'); ?>. <?php _e("All rights reserved.", "wp-inspired"); ?> 
		<?php /* Solostream Footer Credit. */ $link = array(
		"<a href = \"http://www.solostream.com\">WordPress Themes</a>",
		"<a href = \"http://www.solostream.com\">Premium WordPress Themes</a>",
		"<a href = \"http://www.solostream.com\">WordPress Business Themes</a>",
		"<a href = \"http://www.solostream.com\">Business WordPress Themes</a>",
		"<a href = \"http://www.wp-magazine.com\">WordPress Magazine Themes</a>"); 
		srand(time()); 
		$random = (rand()%5); 
		echo ("$link[$random]");
		/* End Solostream Footer Credit. */ ?>

		<?php include (TEMPLATEPATH . '/sub-icons.php'); ?>

	</div>

</div>

<?php wp_footer(); ?>

</body>

</html>