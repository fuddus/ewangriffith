<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

<?php get_header(); ?>

	<div id="slider-position">
		<?php echo do_shortcode("[metaslider id=55]"); ?>
	</div>
	<?php if ( is_home() && $paged < 2 && $wp_inspired_features_on == 'Full Width Featured Content Slider') { ?>
	<?php include (TEMPLATEPATH . '/features.php'); ?>
	<?php } ?>

	<div id="page" class="clearfix">

		<?php include (TEMPLATEPATH . '/banner728.php'); ?>

		<div id="contentleft" class="maincontent">

			<?php if ( is_home() && $paged < 2 && $wp_inspired_features_on == 'Narrow Width Featured Content Slider') { ?>

			<?php } ?>

			<?php if ( $wp_inspired_latest_tweet == 'Yes' ) { ?>
			<?php include (TEMPLATEPATH . '/recent-tweet.php'); ?>
			<?php } ?>

			<?php if ( $wp_inspired_galleries_on == 'Yes' ) { ?>
			<?php include (TEMPLATEPATH . '/galleries.php'); ?>
			<?php } ?>

			<div id="content" class="clearfix">

				<div class="content-top">
					<h2><?php echo stripslashes($wp_inspired_recent_posts_title); ?></h2>
					<a href="<?php bloginfo('rss2_url'); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/content-top-rss.gif" alt="<?php _e("RSS Feed", "wp-inspired"); ?>" title="<?php _e("RSS Feed", "wp-inspired"); ?>" /></a>
				</div>

				<?php include (TEMPLATEPATH . '/banner468.php'); ?>

				<?php if ( $wp_inspired_home_layout == 'Option 1 - Standard Blog Layout') { ?>
				<?php include (TEMPLATEPATH . '/index1.php'); ?>

				<?php } elseif ( $wp_inspired_home_layout == 'Option 2 - Blog Layout With Thumbnails to Left') { ?>
				<?php include (TEMPLATEPATH . '/index2.php'); ?>

				<?php } elseif ( $wp_inspired_home_layout == 'Option 3 - 2 Posts Aligned Side-by-Side') { ?>
				<?php include (TEMPLATEPATH . '/index3.php'); ?>

				<?php } elseif ( $wp_inspired_home_layout == 'Option 4 - Posts Arranged by Category Side-by-Side') { ?>
				<?php include (TEMPLATEPATH . '/index4.php'); ?>

				<?php } elseif ( $wp_inspired_home_layout == 'Option 5 - Posts Arranged by Category Stacked') { ?>
				<?php include (TEMPLATEPATH . '/index5.php'); ?>

				<?php } ?>

			</div>

		</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
