<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

<div id="recent-photos">

	<div class="content-top-twitter">
		<h2><?php echo stripslashes($wp_inspired_galleries_title); ?></h2>
	</div>

	<div class="photos-content clearfix">
<?php 
$count = 1;
$my_query = new WP_Query(array(
	'category_name' => $wp_inspired_galleries_cat,
	'showposts' => $wp_inspired_galleries_count
));
while ($my_query->have_posts()) : $my_query->the_post();
$do_not_duplicate[] = $post->ID; ?>
<?php if ( $count%4 == 0 ) { ?>
		<div class="post right" id="gallery-<?php the_ID(); ?>">
<?php } else { ?>
		<div class="post" id="gallery-<?php the_ID(); ?>">
<?php } ?>
			<?php include (TEMPLATEPATH . "/post-thumb.php"); ?>
			<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Permanent Link to", "wp-inspired"); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>
		</div>
<?php if ( $count%4 == 0 ) { ?>
		<div style="clear:both;"></div>
<?php } ?>
<?php $count = $count + 1 ?>
<?php endwhile; ?>
	</div>

</div>
