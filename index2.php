<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

<?php
if (is_home()) { 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts(array(
	'post__not_in' => $do_not_duplicate,
	'paged' => $paged
)); }
if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div <?php post_class(); ?> id="post-main-<?php the_ID(); ?>">

					<div class="postinfo-left">
						<?php include (TEMPLATEPATH . "/post-thumb.php"); ?>
						<p class="meta author"><?php the_author_posts_link(); ?></p>
						<p class="meta date"><?php the_time( get_option( 'date_format' ) ); ?></p>
						<?php if ('open' == $post->comment_status) { ?> 
							<p class="meta comments"><a href="<?php comments_link(); ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Comments for", "wp-inspired"); ?> <?php the_title(); ?>"><?php _e("Comments", "wp-inspired"); ?> <?php comments_number('(0)','(1)','(%)'); ?></a></p>
						<?php } ?>
					</div>

					<div class="entry basic clearfix">
						<div class="entry-arrow"></div>
						<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Permanent Link to", "wp-inspired"); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>
						<?php if ( $wp_inspired_post_content == 'Excerpts' ) { ?>
						<?php the_excerpt(); ?>
						<?php } else { ?>
						<?php the_content(''); ?>
						<?php } ?>
						<?php if(function_exists('the_tags')) { the_tags('<p class="tags"><strong>'. __('Tags', 'wp-inspired'). ': </strong> ', ', ', '</p>'); } ?>
						<p class="cats"><strong><?php _e('Category', "wp-inspired"); ?></strong>: <?php the_category(', '); ?></p>
						<p><a class="more-link" href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Permanent Link to", "wp-inspired"); ?> <?php the_title(); ?>"><?php _e("View Post", "wp-inspired"); ?></a></p>
					</div>

					<div style="clear:both;"></div>

				</div>

<?php endwhile; endif; ?>

				<?php include (TEMPLATEPATH . "/bot-nav.php"); ?>
