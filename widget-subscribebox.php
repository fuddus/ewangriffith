<?php 
// This starts the Subscribe Box widget.
add_action( 'widgets_init', 'subscribebox_load_widgets' );

function subscribebox_load_widgets() {
	register_widget( 'SubscribeBox_Widget' );
}

class SubscribeBox_Widget extends WP_Widget {

	function SubscribeBox_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'subscribebox', 'description' => __('Adds the Subscribe Box and/or social network icons.', 'wp-inspired') );
		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'subscribebox-widget' );
		/* Create the widget. */
		$this->WP_Widget( 'subscribebox-widget', __('Subscribe Box Widget', 'wp-inspired'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$intro = $instance['intro'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		printf( '<div class="intro">' );

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;

		/* Display intro from widget settings if one was input. */
		if ( $intro )
			printf( '<p>' . __('%1$s', 'wp-inspired') . '</p>', $intro ); ?>

			<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

			<?php if ( $wp_inspired_subscribe_settings == 'Use Google/FeedBurner Email' && $wp_inspired_fb_feed_id ) { ?>

			<form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo $wp_inspired_fb_feed_id; ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
				<input type="hidden" value="<?php echo $wp_inspired_fb_feed_id; ?>" name="uri"/>
				<input type="hidden" name="loc" value="en_US"/>
				<p class="email-form">
					<input type="text" class="sub" name="email" value="<?php _e("email address", "wp-inspired"); ?>" onfocus="if (this.value == '<?php _e("email address", "wp-inspired"); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e("email address", "wp-inspired"); ?>';}" />
					<input type="image" src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif" class="subbutton" alt="<?php _e("submit", "wp-inspired"); ?>" />
				</p>
				<div style="clear:both;"><small><?php _e("Privacy guaranteed. We'll never share your info.", "wp-inspired"); ?></small></div>
			</form>

			<?php } elseif ( $wp_inspired_subscribe_settings == 'Use Alternate Email List Form' && $wp_inspired_alt_email_code ) { ?>

			<?php echo stripslashes($wp_inspired_alt_email_code); ?>

			<?php } ?>

		<?php printf( '</div>' ); ?>

			<ul>
				<li class="feed">
					<a title="<?php _e("Subscribe via RSS Feed", "wp-inspired"); ?>" href="<?php bloginfo('rss2_url'); ?>">
						<span class="title"><?php _e("RSS Feed", "wp-inspired"); ?></span><br />
						<?php bloginfo('rss2_url'); ?>
					</a>
				</li>

				<?php if ( $wp_inspired_twitter_url ) { ?>
				<li class="twitter">
					<a rel="external" title="<?php echo stripslashes($wp_inspired_twitter_link_text); ?>" href="http://www.twitter.com/<?php echo stripslashes($wp_inspired_twitter_url); ?>">
						<span class="title"><?php echo stripslashes($wp_inspired_twitter_link_text); ?></span><br />
						twitter.com/<?php echo stripslashes($wp_inspired_twitter_url); ?>
					</a>
				</li>
				<?php } ?>

				<?php if ( $wp_inspired_facebook_url ) { ?>
				<li class="facebook">
					<a title="<?php echo stripslashes($wp_inspired_facebook_link_text); ?>" rel="external" href="http://www.facebook.com/<?php echo stripslashes($wp_inspired_facebook_url); ?>">
						<span class="title"><?php echo stripslashes($wp_inspired_facebook_link_text); ?></span><br />
						facebook.com/<?php echo stripslashes($wp_inspired_facebook_url); ?>
					</a>
				</li>
				<?php } ?>

				<?php if ( $wp_inspired_linkedin_url ) { ?>
				<li class="linkedin">
					<a title="<?php echo stripslashes($wp_inspired_linkedin_link_text); ?>" rel="external" href="http://www.linkedin.com/in/<?php echo stripslashes($wp_inspired_linkedin_url); ?>">
						<span class="title"><?php echo stripslashes($wp_inspired_linkedin_link_text); ?></span><br />
						linkedin.com/in/<?php echo stripslashes($wp_inspired_linkedin_url); ?>
					</a>
				</li>
				<?php } ?>

				<?php if ( $wp_inspired_flickr_url ) { ?>
				<li class="flickr">
					<a title="<?php echo stripslashes($wp_inspired_flickr_link_text); ?>" rel="external" href="http://www.flickr.com/photos/<?php echo stripslashes($wp_inspired_flickr_url); ?>">
						<span class="title"><?php echo stripslashes($wp_inspired_flickr_link_text); ?></span><br />
						flickr.com/photos/<?php echo stripslashes($wp_inspired_flickr_url); ?>
					</a>
				</li>
				<?php } ?>


				<?php if ( $wp_inspired_youtube_url ) { ?>
				<li class="youtube">
					<a title="<?php echo stripslashes($wp_inspired_youtube_link_text); ?>" rel="external" href="http://www.youtube.com/user/<?php echo stripslashes($wp_inspired_youtube_url); ?>">
						<span class="title"><?php echo stripslashes($wp_inspired_youtube_link_text); ?></span><br />
						youtube.com/user/<?php echo stripslashes($wp_inspired_youtube_url); ?>
					</a>
				</li>
				<?php } ?>

			</ul>

		<?php 
		/* After widget (defined by themes). */
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and intro to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['intro'] = strip_tags( $new_instance['intro'] );

		return $instance;
	}

	function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array( 'title' => __('Subscribe', 'wp-inspired'), 'intro' => __('Enter your email address below to receive updates each time we publish new content.', 'wp-inspired') );

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'wp-inspired'); ?></label>
		<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" /></p>

		<!-- Intro: Text Input -->
		<p><label for="<?php echo $this->get_field_id( 'intro' ); ?>"><?php _e('Introduction:', 'wp-inspired'); ?></label>
		<textarea rows="3" id="<?php echo $this->get_field_id( 'intro' ); ?>" name="<?php echo $this->get_field_name( 'intro' ); ?>" style="width:100%;"><?php echo $instance['intro']; ?></textarea></p>

	<?php
	}
}
?>
