<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

<div id="recentweet">
	<div class="content-top-twitter">
		<h2><?php echo stripslashes($wp_inspired_latest_tweet_text); ?></h2>
	</div>
	<div class="tweet-content-2">
		<div class="tweet-entry">
			<ul id="twitter_update_list"></ul>
		</div>
	</div>
</div>
<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/<?php echo stripslashes($wp_inspired_twitter_url); ?>.json?callback=twitterCallback2&amp;count=1"></script>