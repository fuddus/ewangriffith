<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

				<div class="post-by-2 clearfix">

<?php 
if (is_home()) { 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts(array(
	'post__not_in' => $do_not_duplicate,
	'paged' => $paged
)); }
if (have_posts()) : while (have_posts()) : the_post();
$post_class = ('post-left' == $post_class) ? 'post-right' : 'post-left'; ?>

					<div class="<?php echo $post_class; ?>">

						<div <?php post_class(); ?> id="post-main-<?php the_ID(); ?>">

							<div class="entry">

								<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Permanent Link to", "wp-inspired"); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>

								<?php include (TEMPLATEPATH . "/postinfo.php"); ?>

								<?php include (TEMPLATEPATH . "/post-thumb.php"); ?>

								<?php if ( $wp_inspired_post_content == 'Excerpts' ) { ?>
								<?php the_excerpt(); ?>
								<?php } else { ?>
								<?php the_content(__('', 'wp-inspired')); ?>
								<?php } ?>
	
							</div>

							<p><a class="more-link" href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Permanent Link to", "wp-inspired"); ?> <?php the_title(); ?>"><?php _e("View Post", "wp-inspired"); ?></a></p>

						</div>

					</div>

					<?php if ( $post_class == 'post-right' ) { ?>
					<div class="post-clear"></div>
					<?php } ?>

<?php endwhile; endif; ?>

				</div>

				<?php include (TEMPLATEPATH . "/bot-nav.php"); ?>
