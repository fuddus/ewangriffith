<?php
// This starts the Side Tabs widget.
add_action( 'widgets_init', 'sidetabs_load_widgets' );

function sidetabs_load_widgets() {
	register_widget( 'Sidetabs_Widget' );
}

class Sidetabs_Widget extends WP_Widget {

	function Sidetabs_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'sidetabs', 'description' => __('Adds the Side Tabs box for popular posts, archives, categories and tags.', 'wp-inspired') );
		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'sidetabs-widget' );
		/* Create the widget. */
		$this->WP_Widget( 'sidetabs-widget', __('Side Tabs Widget', 'wp-inspired'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		/* Before widget (defined by themes). */
		echo $before_widget; ?>

		<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/tabs.js"></script>

		<ul class="tabs">
			<li><a href="javascript:tabSwitch_2(1, 4, 'tab_', 'content_');" id="tab_1" class="on"><?php _e("Locate", "wp-inspired"); ?><span></span></a></li>
			<li><a href="javascript:tabSwitch_2(2, 4, 'tab_', 'content_');" id="tab_2"><?php _e("Popular", "wp-inspired"); ?><span></span></a></li>
			<li><a href="javascript:tabSwitch_2(3, 4, 'tab_', 'content_');" id="tab_3"><?php _e("Recent", "wp-inspired"); ?><span></span></a></li>
			<li><a href="javascript:tabSwitch_2(4, 4, 'tab_', 'content_');" id="tab_4"><?php _e("Comments", "wp-inspired"); ?><span></span></a></li>
		</ul>

		<div style="clear:both;"></div>

		<div id="content_1" class="cat_content clearfix">
			<ul class="side-arc">
				<li class="clearfix">
					<p class="title"><?php _e("Search", "wp-inspired"); ?>:</p>
					<?php include (TEMPLATEPATH . "/searchform.php"); ?>
				</li>
				<li class="clearfix">
					<p class="title"><?php _e("Archives", "wp-inspired"); ?>:</p>
					<select name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
						<option value=""><?php echo attribute_escape(__('Select Month')); ?></option>
						<?php wp_get_archives('type=monthly&format=option&show_post_count=1'); ?>
					</select>
					<noscript><input type="submit" value="<?php _e("Go", "wp-inspired"); ?>" /></noscript>
				</li>
				<li class="clearfix">
					<p class="title"><?php _e("Categories", "wp-inspired"); ?>:</p>
					<form action="<?php bloginfo('url'); ?>/" method="get">
						<?php $select = wp_dropdown_categories('show_option_none=' . __('Select Category') .'&show_count=1&orderby=name&echo=0&hierarchical=1');
						$select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
						echo $select;
						?>
						<noscript><input type="submit" value="<?php _e("Go", "wp-inspired"); ?>" /></noscript>
					</form>
				</li>
				<li class="clearfix">
					<p class="title"><?php _e("Tags", "wp-inspired"); ?>:</p>
					<select name="tag-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
						<option value=""><?php echo attribute_escape(__('Select a Tag')); ?></option>
						<?php $posttags = get_tags('orderby=count&order=DESC&number=200'); ?>
						<?php if ($posttags) {
							foreach($posttags as $tag) {
								echo "<option value='";
								echo get_tag_link($tag);
								echo "'>";
								echo $tag->name;
								echo " (";
								echo $tag->count;
								echo ")";
								echo "</option>"; }
							} ?>
					</select>
					<noscript><input type="submit" value="<?php _e("Go", "wp-inspired"); ?>" /></noscript>
				</li>
			</ul>
		</div>

		<div id="content_2" class="cat_content clearfix" style="display:none">
			<?php if ( function_exists('get_mostpopular') ) : ?>
				<div class="popular"><?php get_mostpopular("range=weekly&limit=5&order_by=avg&stats_comments=0&pages=0"); ?></div>
			<?php else : ?>
				<div style="padding:15px 13px 5px;"><?php _e("This feature has not been activated yet.", "wp-inspired"); ?></div>
			<?php endif; ?>
		</div>

		<div id="content_3" class="cat_content clearfix" style="display:none">
			<ul>
				<?php
				$numposts = 5;
				query_posts('showposts='.$numposts); ?>
				<?php while (have_posts()) : the_post(); ?>
				<li>
					<a href="<?php the_permalink() ?>" rel="<?php _e("bookmark"); ?>" title="<?php _e("Permanent Link to"); ?> <?php the_title(); ?>"><?php the_title(); ?></a>
				</li>
				<?php endwhile; ?>
			</ul>
		</div>

		<div id="content_4" class="cat_content clearfix side-recent-comments" style="display:none">
		<?php
			$pre_HTML ="";
			$post_HTML ="";
			global $wpdb;
			$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_author_email, comment_date_gmt, comment_approved, comment_type,comment_author_url, SUBSTRING(comment_content,1,100) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT 5";

			$comments = $wpdb->get_results($sql);
			$output = $pre_HTML;
			$output .= "\n<ul>";
			foreach ($comments as $comment) {
				$output .= "\n<li><a href=\"" . get_permalink($comment->ID)."#comment-" . $comment->comment_ID . "\" title=\"on ".$comment->post_title . "\">" . strip_tags($comment->comment_author) .": " . strip_tags($comment->com_excerpt)."</a></li>";
			}
			$output .= "\n</ul>";
			$output .= $post_HTML;
			echo $output;
		?>
		</div>

		<?php /* After widget (defined by themes). */
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		return $instance;
	}
}
?>
