<?php
/*
Template Name: Site Map
*/
?>

<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

<?php get_header(); ?>

	<div id="page" class="clearfix">

		<?php include (TEMPLATEPATH . '/banner728.php'); ?>

		<div id="contentleft">

			<div id="content" class="maincontent">

				<div class="content-top">
					<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>
				</div>

				<?php include (TEMPLATEPATH . '/banner468.php'); ?>

				<h1 class="archive-title"><?php the_title(); ?></h1>

				<div class="post maincontent singlepage sitemap clearfix">

					<div class="entry clearfix">

						<div class="sitemap-narrow">

							<h2><span><?php _e("Site Feeds", "wp-inspired"); ?></span></h2>
							<ul class="archives">
								<li><a href="<?php bloginfo('rss2_url'); ?>"><?php _e("Main RSS Feed", "wp-inspired"); ?></a></li>
								<li><a href="<?php bloginfo('comments_rss2_url'); ?>"><?php _e("Comments RSS Feed", "wp-inspired"); ?></a></li>
							</ul>

							<h2><span><?php _e("Pages", "wp-inspired"); ?></span></h2>
							<ul class="archives">
								<li><a href="<?php bloginfo('home'); ?>"><?php _e("Home", "wp-inspired"); ?></a></li>
								<?php wp_list_pages('title_li='); ?>
							</ul>

							<h2><span><?php _e("Monthly Archives", "wp-inspired"); ?></span></h2>
							<ul class="archives">
								<?php wp_get_archives('show_post_count=1'); ?>
							</ul>
		
							<h2><span><?php _e("Categories", "wp-inspired"); ?></span></h2>
							<ul class="archives">
								<?php wp_list_categories('title_li=&show_count=1'); ?>
							</ul>

							<h2><span><?php _e("Top 20 Tags", "wp-inspired"); ?></span></h2>
							<?php wp_tag_cloud('number=20&smallest=9&largest=9&format=list&orderby=count&order=DESC'); ?> 

						</div> <!-- end sitemap-narrow div -->

						<div class="sitemap-wide">

							<h2><span><?php _e("All Articles", "wp-inspired"); ?></span></h2>
<?php
$numposts = 30; 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts('showposts='.$numposts.'&paged=' . $paged); ?>
<?php while (have_posts()) : the_post(); ?>

							<div class="sitemap-post" id="post-<?php the_ID(); ?>">
								<p><strong><a href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Permanent Link to", "wp-inspired"); ?> <?php the_title(); ?>"><?php the_title(); ?></a></strong><br />
								<?php the_time( get_option( 'date_format' ) ); ?> | <?php the_author_posts_link(); ?> | <a href="<?php comments_link(); ?>" rel="<?php _e("bookmark", "wp-inspired"); ?>" title="<?php _e("Comments for", "wp-inspired"); ?> <?php the_title(); ?>"><?php comments_number(__('0 Comments', 'wp-inspired'), __('1 Comments', 'wp-inspired'), __( '% Comments', 'wp-inspired'));?></a></p>

							</div>
<?php endwhile; ?>
						</div> <!-- end sitemap-wide div -->

					</div> <!-- end entry div -->

				</div> <!-- end post div -->

				<?php include (TEMPLATEPATH . '/bot-nav.php'); ?>
				
			</div> <!-- end content div -->

		</div> <!-- end contentleft div -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
