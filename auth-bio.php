<?php global $options; foreach ($options as $value) { if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } } ?>

<?php if ( $wp_inspired_show_auth_bio == 'yes' ) { ?>
<div class="auth-bio clearfix">
	<div class="bio">
		<?php // this is the author photo pulled from gravatar.com  
		if (function_exists('get_avatar')) {
		$gravsize = $wp_inspired_grav_size; 
		$author_email = get_the_author_email();
		echo get_avatar($author_email,$size="$gravsize"); } ?>
		<h3 style="margin:0;"><?php _e("About the Author", "wp-inspired"); ?></h3>
		<?php the_author_meta('description'); ?><br /><br />
		<strong><a href="<?php bloginfo('url'); ?>/author/<?php the_author_meta('user_login'); ?>/"><?php _e("View Author Profile", "wp-inspired"); ?></a></strong>
	</div>
</div>
<?php } ?>
